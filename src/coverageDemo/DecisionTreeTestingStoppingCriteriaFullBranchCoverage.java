package coverageDemo;

import static org.testng.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import org.jacoco.agent.rt.IAgent;
import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.ICounter;
import org.jacoco.core.analysis.IMethodCoverage;
import org.jacoco.core.data.ExecutionDataReader;
import org.jacoco.core.data.ExecutionDataStore;
import org.jacoco.core.data.SessionInfoStore;
import org.testng.annotations.Test;

/**
 * Class to measure the black box and white box coverage using Decision tree
 * technique (with stopping criteria = full branch coverage) 
 * at the end of entire execution. This experiment is repeated 1000
 * times to minimize the value of standard deviation.
 */
public class DecisionTreeTestingStoppingCriteriaFullBranchCoverage {
	static enum CoverageCriteria {
		$DECISIONTREECOVERAGE, LINESCOVERAGE, INSTRUCTIONSCOVERAGE, BRANCHESCOVERAGE, $$EXECUTIONTIME, $$$RUNTIMENUMBER,
		$$NUMBEROFTESTS
	};

	// Meanings of ICounter status values
	static String meaning[] = new String[4];
	{
		meaning[ICounter.EMPTY] = "EMPTY";
		meaning[ICounter.NOT_COVERED] = "NOT COVERED";
		meaning[ICounter.FULLY_COVERED] = "FULLY COVERED";
		meaning[ICounter.PARTLY_COVERED] = "PARTLY COVERED";
	}

	/**
	 * Method to measure the black box and white box coverage using Decision tree
	 * technique (with stopping criteria = full branch coverage) 
	 * at the end of each run execution.
	 */
	
	@Test
	public static void randomtestingWithDecisionTree() throws Exception {
		List<List<Double>> rows = new ArrayList<List<Double>>();
		int[] results = { 0, 200, 300, 400, 500, 2000 };
		char[] genders = { 'M', 'F' };
		char[] gendersinvalid = { 'M', 'F', 'X' };

		FileWriter csvWriter = new FileWriter("DecisionTreeTestingStoppingCriteriaFullBranchCoverage.csv");
		for (int runtimeNumber = 0; runtimeNumber < 1000; runtimeNumber++) {
			double decisionTreeCoverage = 0.0;
			int decisionTreeCoveredCount = 0;
			int testCaseNumber = 0;
			
			/*
			 * Initializing decision tree array to mark all the decision tree paths as
			 * uncovered.
			 */
			
			boolean decisionTree[] = new boolean[12];
			for (int i = 0; i < 12; i++) {
				decisionTree[i] = false;
			}

			// counters
			long startTime;
			int fails = 0;

			Random r_results = new Random();
			Random r_gender = new Random();
			Random r_married = new Random();
			Random r_age = new Random();
			Map<String, Double> criteriaMap = new TreeMap<String, Double>();
			startTime = System.nanoTime();

			int loop = 0;
			Map<String, Double> whiteBoxCoverage = new HashMap<String, Double>();
			whiteBoxCoverage.put(CoverageCriteria.BRANCHESCOVERAGE.toString(), 0.0);
			while (whiteBoxCoverage.get(CoverageCriteria.BRANCHESCOVERAGE.toString()) < 96.15384615384616) {

				// define test data variables here
				long age = -1;
				char gender = 'X';
				boolean married = false;
				int expected = 0;
				
				/*
				 * generate random test values 
				 * Select premium at random (0,200,300,400,500,2000)
				 * If 2000, 
				 * select age at random from 16..24 
				 * select married=false 
				 * select gender = 'M'
				 */

				expected = results[r_results.nextInt(results.length)];			
				if (expected == 2000) {
					if (decisionTree[0] == false) {
						decisionTree[0] = true;
						decisionTreeCoveredCount++;

					}

					age = 16 + r_age.nextInt(24 - 15);
					married = false;
					gender = genders[0];

				} 
				
				/* If 500,
				 * select age at random from 25..44
				 * select married=false
				 * select gender='M'
				 */
				
				else if (expected == 500) {
					if (decisionTree[1] == false) {
						decisionTree[1] = true;
						decisionTreeCoveredCount++;

					}
					age = 25 + r_age.nextInt(44 - 24);
					married = false;
					gender = genders[0];

				} 
				
				/*If 400,
				 * select age at random from 45..65
				 * select married=false
				 * select gender='M'
				 * 
				 */
				
				else if (expected == 400) {
					if (decisionTree[2] == false) {
						decisionTree[2] = true;
						decisionTreeCoveredCount++;

					}
					age = 45 + r_age.nextInt(65 - 44);
					married = false;
					gender = genders[0];

				} 
				
				/*
				 *If 300, select married at random,
				 * If true, 
				 * 		select age at random from 16..44
				 * 		select gender at random from ('M','F') 
				 * If false,
				 * 		select age at random from 16..4
				 * 		select gender= 'F'
				 */
				
				else if (expected == 300) {
					if (decisionTree[3] == false) {
						decisionTree[3] = true;
						decisionTreeCoveredCount++;

					}
					married = r_married.nextBoolean();
					if (!married) {
						if (decisionTree[4] == false) {
							decisionTree[4] = true;
							decisionTreeCoveredCount++;

						}
						age = 16 + r_age.nextInt(44 - 15);
						gender = genders[1];
					} else {
						if (decisionTree[5] == false) {
							decisionTree[5] = true;
							decisionTreeCoveredCount++;

						}
						age = 16 + r_age.nextInt(44 - 15);
						gender = genders[r_gender.nextInt(genders.length)];
					}
				}
				
				/*
				 *If 200, select married at random,
				 * If true, 
				 * 		select age at random from 45..65
				 * 		select gender at random from ('M','F') 
				 * If false,
				 * 		select age at random from 45..65
				 * 		select gender= 'F'
				 */
				
				else if (expected == 200) {
					if (decisionTree[6] == false) {
						decisionTree[6] = true;
						decisionTreeCoveredCount++;

					}
					married = r_married.nextBoolean();
					if (!married) {
						if (decisionTree[7] == false) {
							decisionTree[7] = true;
							decisionTreeCoveredCount++;

						}
						age = 45 + r_age.nextInt(65 - 44);
						gender = genders[1];
					} else {
						if (decisionTree[8] == false) {
							decisionTree[8] = true;
							decisionTreeCoveredCount++;

						}
						age = 45 + r_age.nextInt(65 - 44);
						gender = genders[r_gender.nextInt(genders.length)];
					}
				}

				else { 
					
					/*
					 *If 0, select gender at random from ('M','F' and 'X'),
					 * If 'X', 
					 * 		select age at random from 16..65
					 * 		select married at random 
					 * If false,
					 * 		select age at random from 0..15 and 66..Integer.MAX
					 * 		select married at random 
					 */
					

					if (decisionTree[9] == false) {
						decisionTree[9] = true;
						decisionTreeCoveredCount++;

					}
					gender = gendersinvalid[r_gender.nextInt(gendersinvalid.length)];
					if (gender == 'X') {
						if (decisionTree[10] == false) {
							decisionTree[10] = true;
							decisionTreeCoveredCount++;

						}
						age = 16 + r_age.nextInt(65 - 15);
						married = r_married.nextBoolean();
					} else {
						if (decisionTree[11] == false) {
							decisionTree[11] = true;
							decisionTreeCoveredCount++;

						}
						do {
							age = r_age.nextInt();
						} while (!(age > 65 || age < 16));

						married = r_married.nextBoolean();
						gender = genders[r_gender.nextInt(genders.length)];

					}

				}

				
				if (Insurance.premium(age, gender, married) != expected) {
					fails++;
					System.out.println("Error log: check(" + age + "," + married + " produced " + gender + ""
							+ Insurance.premium(age, gender, married) + "not expected: " + expected + " on loop "
							+ testCaseNumber);
				}
				
				//Checking the correctness of the code
				
				assertEquals(Insurance.premium(age, gender, married), expected);

				decisionTreeCoverage = (Double.valueOf(decisionTreeCoveredCount) / Double.valueOf(decisionTree.length))
						* 100;

				loop++;
				whiteBoxCoverage = printCoverage("coverageDemo.Insurance", "premium", false);

			}
			if (fails > 0)
				System.out.println("Test failed");

			criteriaMap.put(CoverageCriteria.$DECISIONTREECOVERAGE.toString(), decisionTreeCoverage);
			criteriaMap.put(CoverageCriteria.$$NUMBEROFTESTS.toString(), Double.valueOf(loop));
			criteriaMap.put(CoverageCriteria.$$EXECUTIONTIME.toString(), Double.valueOf(System.nanoTime() - startTime));
			whiteBoxCoverage = printCoverage("coverageDemo.Insurance", "premium", true);
			criteriaMap.put(CoverageCriteria.$$$RUNTIMENUMBER.toString(), Double.valueOf(runtimeNumber + 1));

			criteriaMap.put(CoverageCriteria.LINESCOVERAGE.toString(),
					Double.valueOf(whiteBoxCoverage.get(CoverageCriteria.LINESCOVERAGE.toString())));
			criteriaMap.put(CoverageCriteria.INSTRUCTIONSCOVERAGE.toString(),
					Double.valueOf(whiteBoxCoverage.get(CoverageCriteria.INSTRUCTIONSCOVERAGE.toString())));
			criteriaMap.put(CoverageCriteria.BRANCHESCOVERAGE.toString(),
					Double.valueOf(whiteBoxCoverage.get(CoverageCriteria.BRANCHESCOVERAGE.toString())));

			List<Double> criteriaValueList = new ArrayList<>(criteriaMap.values());
			rows.add(criteriaValueList);

		}
		csvWriter.append("RunTimeNumber");
		csvWriter.append(",");
		csvWriter.append("Execution time");
		csvWriter.append(",");
		csvWriter.append("Number Of Tests");
		csvWriter.append(",");
		csvWriter.append("Decision Tree Coverage");
		csvWriter.append(",");
		csvWriter.append("Branches Coverage");
		csvWriter.append(",");
		csvWriter.append("Instructions Coverage");
		csvWriter.append(",");
		csvWriter.append("Lines Coverage");
		csvWriter.append("\n");
		for (List<Double> rowData : rows) {
			csvWriter.append(String.join(",", rowData.toString().substring(1, rowData.toString().length() - 1)));
			csvWriter.append("\n");
		}

		csvWriter.flush();
		csvWriter.close();

	}


	/**
	 * Method to print the white box coverage
	 * 
	 * @param className
	 * @param methodName
	 * @param refreshExcutiondata
	 * @return
	 * @throws Exception
	 */
	
	public static Map<String, Double> printCoverage(String className, String methodName, boolean refreshCoverage)
			throws Exception {
		Map<String, Double> whiteboxCoverageMap = new HashMap<String, Double>();

		try {

			IAgent agent = org.jacoco.agent.rt.RT.getAgent();
			byte[] executionData = agent.getExecutionData(refreshCoverage);
			CoverageBuilder coverageBuilder = new CoverageBuilder();
			InputStream myInputStream = new ByteArrayInputStream(executionData);
			final ExecutionDataReader reader = new ExecutionDataReader(myInputStream);
			final SessionInfoStore sessionInfoStore = new SessionInfoStore();
			final ExecutionDataStore executionDataStore = new ExecutionDataStore();
			reader.setSessionInfoVisitor(sessionInfoStore);
			reader.setExecutionDataVisitor(executionDataStore);
			reader.read();
			Analyzer analyzer = new Analyzer(executionDataStore, coverageBuilder);
			className = className.replace(".", "/");
			String classFileName = "./bin/" + className + ".class";
			File classFile = new File(classFileName);
			if (!classFile.exists())
				System.out.println("Error: class file not found");
			analyzer.analyzeAll(classFile);
			Collection<IClassCoverage> cc = coverageBuilder.getClasses();

			for (IClassCoverage c : cc) {
				
				for (IMethodCoverage m : c.getMethods()) {
					if (methodName.equals(m.getName())) {

						Double linesCoverage = (Double) (m.getLineCounter().getCoveredCount() / Double
								.valueOf((m.getLineCounter().getCoveredCount() + m.getLineCounter().getMissedCount())))
								* 100;						
						Double instructionsCoverage = (Double) (m.getInstructionCounter().getCoveredCount()
								/ Double.valueOf((m.getInstructionCounter().getCoveredCount()
										+ m.getInstructionCounter().getMissedCount())))
								* 100;

						Double branchCoverage = (Double) (m.getBranchCounter().getCoveredCount() / Double.valueOf(
								(m.getBranchCounter().getCoveredCount() + m.getBranchCounter().getMissedCount())))
								* 100;

						whiteboxCoverageMap.put(CoverageCriteria.LINESCOVERAGE.toString(), linesCoverage);
						whiteboxCoverageMap.put(CoverageCriteria.INSTRUCTIONSCOVERAGE.toString(), instructionsCoverage);
						whiteboxCoverageMap.put(CoverageCriteria.BRANCHESCOVERAGE.toString(), branchCoverage);

					} else {
						
					}
				}
			}

		} catch (Exception ex) {

			throw ex;
		}
		return whiteboxCoverageMap;

	}

}
