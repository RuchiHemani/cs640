package coverageDemo;

import static org.testng.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;

import org.jacoco.agent.rt.IAgent;
import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.IMethodCoverage;
import org.jacoco.core.data.ExecutionDataReader;
import org.jacoco.core.data.ExecutionDataStore;
import org.jacoco.core.data.SessionInfoStore;
import org.testng.annotations.Test;

/**
 * Class to test SUT using directed automated random walkthrough of the state
 * diagram using All States strategy.
 */

public class DARWSDAllStates {
	static enum Criteria {
		$$$RUNTIMENUMBER, $$NUMBEROFFAULTS, $NUMBEROFTESTCASESTOGETALLSTATESCOVERAGE,
		$TIMEINNANOSECONDSTOGETALLSTATESCOVERAGE, $LINESCOVERAGE, $INSTRUCTIONSCOVERAGE, $BRANCHESCOVERAGE,
		$$TRANSITIONCOVERAGE, $$STATESCOVERAGE
	};

	int[] events = { 0, 1, 2, 3 };
	int[][] transitionTable = { { 1, 0, 0, 2 }, { 1, 0, 1, 3 }, { 3, 2, 0, 2 }, { 3, 2, 1, 3 } };
	ACControllerCode accontrollerCode;

	/**
	 * Method to test SUT using directed automated random walkthrough of the state
	 * diagram using All Transitions strategy.
	 */

	@Test
	public void acControllerTest() throws Exception {
		List<List<Double>> rows = new ArrayList<List<Double>>();
		Scanner scan = new Scanner(System.in);
		System.out.println("Select 1 for Running Code with Built-in test");
		System.out.println("Select 2 for Running Code without Built-in test");
		System.out.println("Select 3 for Running Fixed Code");
		String option = scan.next();

		// Initializing csv file based on user input to collect the results

		FileWriter csvWriter;
		if (option.equalsIgnoreCase("1")) {
			csvWriter = new FileWriter("DARWSD_AllStates_ACControllerWithBuiltInTest.csv");

		} else if (option.equalsIgnoreCase("2")) {

			csvWriter = new FileWriter("DARWSD_AllStates_ACControllerWithoutBuiltInTest.csv");

		} else if (option.equalsIgnoreCase("3")) {

			csvWriter = new FileWriter("DARWSD_AllStates_ACControllerFixedCode.csv");
		} else {
			System.out.println("Please enter valid choice");
			csvWriter = new FileWriter("InvalidOptionSelected.csv");
		}

		int totalRunTime = 1000;
		long startTime, endTime;

		// Running 1000 times to minimize the standard deviation

		for (int runTimeNumber = 0; runTimeNumber < totalRunTime; runTimeNumber++) {

			// Initialization

			boolean[][] transitions = { { false, false, false, false }, { false, false, false, false },
					{ false, false, false, false }, { false, false, false, false } };

			boolean[] states = { false, false, false, false };
			int totalNumberOfTransitions = transitions.length * transitions[transitions.length - 1].length;
			int numberOfFaults = 0;
			int expectedNextState = 0;
			int state = 0;
			int numberOfStatesCovered = 0;
			int numberOfTransitionCovered = 0;

			int event = 0;
			int numberOfTestCases = 0;
			List<Boolean> statusList = new ArrayList<Boolean>();
			int actualStatus = 0;
			startTime = System.nanoTime();
			// System.out.println("StartTime=" + startTime);
			if (option.equalsIgnoreCase("1")) {
				accontrollerCode = new ACControllerWithBuiltInTest();

			} else if (option.equalsIgnoreCase("2")) {
				accontrollerCode = new ACControllerWithoutBuiltInTest();

			} else if (option.equalsIgnoreCase("3")) {
				accontrollerCode = new ACControllerFixedCode();

			} else {
				accontrollerCode = null;

			}
			Map<String, Double> criteriaMap = new TreeMap<String, Double>();

			for (int i = 0; i < transitions.length; i++) {
				states[i] = false;
				for (int j = 0; j < transitions[i].length; j++) {
					transitions[i][j] = false;
				}
			}

			int unCoveredState = 0;

			List<Integer> pathToReachUncoveredTransition = new ArrayList<Integer>();
			states[state] = true;
			numberOfStatesCovered = 1;

			// Iterating until the stopping criteria is false
			while (numberOfStatesCovered < states.length) {

				/*
				 * If there are no events in the path list to be traversed to reach either the
				 * uncovered state or the state with uncovered transition based on the stopping
				 * criteria. Or in simple terms, there are uncovered transitions from the
				 * current state
				 */

				if (pathToReachUncoveredTransition.isEmpty()) {
					event = getEvent(state, states);
				}

				/*
				 * if getEvent() returns -1, indicating all the events are covered from the
				 * current state but still the stopping criteria is false.
				 */

				if (event == -1 && (numberOfStatesCovered < states.length)) {

					/*
					 * getPath() method is called to get a list of events to be triggered to reach
					 * either the uncovered state or the state with uncovered transition.
					 */

					pathToReachUncoveredTransition = getPath(state, unCoveredState, states);
				}

				/*
				 * Following condition is to check if there exists a path from the current state
				 * to either the uncovered state or the state with uncovered transition based on
				 * the stopping criteria.
				 */

				if (!pathToReachUncoveredTransition.isEmpty()) {

					/*
					 * If the pathToReachUncoveredTransition list is not empty, last element of the
					 * pathToReachUncoveredTransition(since the messages were added recursively in
					 * the list in getPath() method) is assigned to the message and that element is
					 * removed from the list.
					 */

					event = pathToReachUncoveredTransition.get(pathToReachUncoveredTransition.size() - 1);
					pathToReachUncoveredTransition.remove(pathToReachUncoveredTransition.size() - 1);
				}

				numberOfTestCases++;

				// Based on the transition table, expectedNextState is assigned to check for the
				// correctness

				expectedNextState = transitionTable[state][event];

				try {

					/*
					 * Software Under Test(SUT) is called by passing the generated message It is
					 * assumed that SUT returns a statusList containing the variables determining
					 * the state of the object
					 */

					statusList = accontrollerCode.ac_controller(event);

					actualStatus = getStatus(statusList);

					assertEquals(actualStatus, expectedNextState);
				}
				/*
				 * Catch block for catching two types of faults
				 */
				catch (AssertionError e) {

					if (transitions[state][event] == false) {
						numberOfFaults++;
						criteriaMap.put(numberOfFaults + "TIMEINNANOSECONDSTOFINDFAULT" + numberOfFaults,
								Double.valueOf(System.nanoTime() - startTime));
						criteriaMap.put(numberOfFaults + "NUMBEROFTESTCASESTOFINDFAULT" + numberOfFaults,
								Double.valueOf(numberOfTestCases));

					}
					setStatus(state, event, accontrollerCode);
				} catch (Exception e) {

					if (transitions[state][event] == false) {
						numberOfFaults++;
						criteriaMap.put(numberOfFaults + "TIMEINNANOSECONDSTOFINDFAULT" + numberOfFaults,
								Double.valueOf(System.nanoTime() - startTime));
						criteriaMap.put(numberOfFaults + "NUMBEROFTESTCASESTOFINDFAULT" + numberOfFaults,
								Double.valueOf(numberOfTestCases));

					}
					setStatus(state, event, accontrollerCode);
				}

				/*
				 * Transition and states array is iterated to mark the covered states and
				 * transitions
				 */

				if (states[transitionTable[state][event]] == false) {
					states[transitionTable[state][event]] = true;
					numberOfStatesCovered++;
				}
				if (transitions[state][event] == false) {
					transitions[state][event] = true;
					numberOfTransitionCovered++;
				}
				unCoveredState = getUnCoveredStateRandomly(states);
				state = expectedNextState;

			}
			endTime = System.nanoTime();

			Double allStatesCoverage = ((Double) (numberOfStatesCovered / Double.valueOf(states.length))) * 100;
			Double allTransitionsCoverage = ((Double) (numberOfTransitionCovered
					/ Double.valueOf(totalNumberOfTransitions))) * 100;

			criteriaMap.put(Criteria.$$TRANSITIONCOVERAGE.toString(), allTransitionsCoverage);
			criteriaMap.put(Criteria.$$STATESCOVERAGE.toString(), allStatesCoverage);

			Map<String, Double> whiteBoxCoverageMap = printCoverage(accontrollerCode.getClass().getName(),
					"ac_controller", true);
			criteriaMap.put(Criteria.$$$RUNTIMENUMBER.toString(), Double.valueOf(runTimeNumber + 1));
			criteriaMap.put(Criteria.$TIMEINNANOSECONDSTOGETALLSTATESCOVERAGE.toString(),
					Double.valueOf(endTime - startTime));
			criteriaMap.put(Criteria.$NUMBEROFTESTCASESTOGETALLSTATESCOVERAGE.toString(),
					Double.valueOf(numberOfTestCases));
			criteriaMap.put(Criteria.$LINESCOVERAGE.toString(),
					Double.valueOf(whiteBoxCoverageMap.get(Criteria.$LINESCOVERAGE.toString())));
			criteriaMap.put(Criteria.$INSTRUCTIONSCOVERAGE.toString(),
					Double.valueOf(whiteBoxCoverageMap.get(Criteria.$INSTRUCTIONSCOVERAGE.toString())));
			criteriaMap.put(Criteria.$BRANCHESCOVERAGE.toString(),
					Double.valueOf(whiteBoxCoverageMap.get(Criteria.$BRANCHESCOVERAGE.toString())));
			criteriaMap.put(Criteria.$$NUMBEROFFAULTS.toString(), Double.valueOf(numberOfFaults));

			List<Double> criteriaValueList = new ArrayList<>(criteriaMap.values());
			rows.add(criteriaValueList);

		}
		csvWriter.append("RunTimeNumber");
		csvWriter.append(",");
		csvWriter.append("Number Of Faults");
		csvWriter.append(",");
		csvWriter.append("States Coverage");
		csvWriter.append(",");
		csvWriter.append("Transition Coverage");
		csvWriter.append(",");
		csvWriter.append("Branches Coverage");
		csvWriter.append(",");
		csvWriter.append("Instructions Coverage");
		csvWriter.append(",");
		csvWriter.append("Lines Coverage");
		csvWriter.append("\n");
		for (List<Double> rowData : rows) {
			csvWriter.append(String.join(",", rowData.toString().substring(1, rowData.toString().length() - 1)));
			csvWriter.append("\n");
		}

		csvWriter.flush();
		csvWriter.close();
		scan.close();

	}

	/**
	 * Method to get the uncovered state using random search.
	 */

	public int getUnCoveredStateRandomly(boolean states[]) {
		Random r_uncoveredState = new Random();
		boolean[] statesTraversedRandomly = { false, false, false, false };
		int numberOfStatesTraversedRandomly = 0;
		int randomState = 0;
		while (numberOfStatesTraversedRandomly < states.length) {
			randomState = r_uncoveredState.nextInt(states.length);
			if (states[randomState] == false) {
				return randomState;
			}
			if (statesTraversedRandomly[randomState] == false) {
				statesTraversedRandomly[randomState] = true;
				numberOfStatesTraversedRandomly++;
			}

		}
		return -1;
	}

	/**
	 * Method to get the state of the object using state diagram
	 **/
	public int getStatus(List<Boolean> statusList) {
		if (statusList.get(0) == false && statusList.get(1) == false && statusList.get(2) == false) {
			return 0;
		} else if (statusList.get(0) == true && statusList.get(1) == false && statusList.get(2) == false) {
			return 1;
		} else if (statusList.get(0) == false && statusList.get(1) == true && statusList.get(2) == false) {
			return 2;

		} else if (statusList.get(0) == true && statusList.get(1) == true && statusList.get(2) == true) {
			return 3;

		}
		return -1;
	}

	/**
	 * Method to get only the uncovered transitions from the current state.
	 * 
	 * @param state= current State
	 * @return uncovered message from the current state if all the messages are
	 *         covered from the current state, returns -1.
	 **/

	public int getEvent(int state, boolean[] states) {

		// Initializing random number generator for the event

		Random r_event = new Random();
		boolean eventsCovered[] = { false, false, false, false };
		int numberOfeventsCovered = 0;

		/*
		 * Traversing until all the possible events from the current state are checked
		 * whether they are covered or not
		 */

		while (numberOfeventsCovered < events.length) {
			int unCoveredEvent = r_event.nextInt(events.length);

			if (states[transitionTable[state][unCoveredEvent]] == false)
				return unCoveredEvent;

			if (eventsCovered[unCoveredEvent] == false) {
				eventsCovered[unCoveredEvent] = true;
				numberOfeventsCovered++;
			}
		}
		return -1;
	}

	/**
	 * Method to return a list of messages to be passed in reverse order to reach
	 * the state with uncovered transition from the current state.
	 * 
	 * @param state= current State, unCoveredTransitionState= state with uncovered
	 *        transition
	 * @return List of integers containing the messages to be passed in reverse
	 *         order to reach the state with uncovered transition.
	 **/

	public List<Integer> getPath(int currentState, int destinationState, boolean[] states) {
		List<Integer> pathList = new ArrayList<Integer>();
		int numberOfStatesTraversed = 0;

		/*
		 * Initializing the stateTobeTraversed from the current state to reach the state
		 * with uncovered transition as the current state Or in simple terms, assuming
		 * that there is a direct path from the current state to the state with
		 * uncovered transition and there is no need to traverse any other state in
		 * between
		 */

		int stateToBeTraversed = currentState;
		boolean[] statesTraversed = { false, false, false, false };
		Random r_stateToBeTraversed = new Random();
		Random r_eventToBeTriggered = new Random();

		// Iterating until all the states are traversed

		while (numberOfStatesTraversed < states.length) {
			int numberOfEventsTriggered = 0;
			int messageToBeTriggered = 0;
			boolean[] messagesTriggered = { false, false, false, false };
			while (numberOfEventsTriggered < events.length) {
				messageToBeTriggered = r_eventToBeTriggered.nextInt(events.length);

				if (messagesTriggered[messageToBeTriggered] == false) {
					messagesTriggered[messageToBeTriggered] = true;
					numberOfEventsTriggered++;
				}

				/*
				 * Condition to check if the state with uncovered transition can be reached from
				 * the current state(state in the current recursion call) by triggering the
				 * event messageToBeTriggered
				 */

				if (transitionTable[stateToBeTraversed][messageToBeTriggered] == destinationState
						&& stateToBeTraversed == currentState && stateToBeTraversed != destinationState) {

					/*
					 * If state with uncovered transition can be reached from the current state in
					 * the current recursion call by triggering the message messageToBeTriggered,
					 * that message is added to the pathList and the pathList is returned.
					 */

					pathList.add(messageToBeTriggered);
					return pathList;
				}

				/*
				 * If no message can trigger the transition from the current state in the
				 * current recursion call to the state with uncovered transition
				 */

				else if (transitionTable[stateToBeTraversed][messageToBeTriggered] == destinationState
						&& stateToBeTraversed != currentState && stateToBeTraversed != destinationState) {
					pathList.add(messageToBeTriggered);

					/*
					 * getPath() method is recursively called until the direct path to the state
					 * with uncovered transition is found.
					 */

					pathList.add(getPath(currentState, stateToBeTraversed, states).get(0));
					return pathList;
				}
			}

			// State to be traversed is randomly generated

			stateToBeTraversed = r_stateToBeTraversed.nextInt(states.length);
			if (statesTraversed[stateToBeTraversed] == false) {
				statesTraversed[stateToBeTraversed] = true;
				numberOfStatesTraversed++;
			}

		}

		// List of events to be triggered is returned
		return pathList;

	}

	/**
	 * Method to explicitly correct the state of the system in case a fault is
	 * encountered.
	 * 
	 * @param state
	 * @param message
	 * @param accontrollerCode
	 */

	public void setStatus(int state, int message, ACControllerCode accontrollerCode2) {

		switch (state) {
		case 0: {
			switch (message) {
			case 0: {
				accontrollerCode2.setIs_room_hot(true);
				break;
			}
			case 3: {
				accontrollerCode2.setIs_door_closed(true);
				break;
			}
			}
			break;
		}
		case 1: {
			switch (message) {
			case 1: {
				accontrollerCode2.setIs_room_hot(false);
				break;
			}
			case 3: {
				accontrollerCode2.setIs_door_closed(true);
				accontrollerCode2.setAc(true);
				break;
			}
			}
			break;
		}
		case 2: {
			switch (message) {
			case 0: {
				accontrollerCode2.setIs_room_hot(true);
				accontrollerCode2.setAc(true);
				break;
			}
			case 2: {
				accontrollerCode2.setIs_door_closed(false);

				break;
			}
			}
			break;
		}
		case 3: {
			switch (message) {
			case 1: {
				accontrollerCode2.setIs_room_hot(false);
				accontrollerCode2.setAc(false);
				break;
			}
			case 2: {
				accontrollerCode2.setIs_door_closed(false);
				accontrollerCode2.setAc(false);
				break;
			}
			}
			break;
		}
		}
	}

	/**
	 * Method to print the white box coverage
	 * 
	 * @param className
	 * @param methodName
	 * @param refreshExcutiondata
	 * @return
	 * @throws Exception
	 */

	public Map<String, Double> printCoverage(String className, String methodName, boolean refreshExcutiondata)
			throws Exception {
		Map<String, Double> whiteboxCoverageMap = new HashMap<String, Double>();
		try {

			IAgent agent = org.jacoco.agent.rt.RT.getAgent();
			byte[] executionData = agent.getExecutionData(refreshExcutiondata);
			CoverageBuilder coverageBuilder = new CoverageBuilder();
			InputStream myInputStream = new ByteArrayInputStream(executionData);
			final ExecutionDataReader reader = new ExecutionDataReader(myInputStream);
			final SessionInfoStore sessionInfoStore = new SessionInfoStore();
			final ExecutionDataStore executionDataStore = new ExecutionDataStore();
			reader.setSessionInfoVisitor(sessionInfoStore);
			reader.setExecutionDataVisitor(executionDataStore);
			reader.read();
			Analyzer analyzer = new Analyzer(executionDataStore, coverageBuilder);
			className = className.replace(".", "/");
			String classFileName = "./bin/" + className + ".class";
			File classFile = new File(classFileName);
			if (!classFile.exists())
				System.out.println("Error: class file not found");
			analyzer.analyzeAll(classFile);
			Collection<IClassCoverage> cc = coverageBuilder.getClasses();

			for (IClassCoverage c : cc) {
				for (IMethodCoverage m : c.getMethods()) {
					if (methodName.equals(m.getName())) {
						Double linesCoverage = (Double) (m.getLineCounter().getCoveredCount() / Double
								.valueOf((m.getLineCounter().getCoveredCount() + m.getLineCounter().getMissedCount())))
								* 100;
						Double instructionsCoverage = (Double) (m.getInstructionCounter().getCoveredCount()
								/ Double.valueOf((m.getInstructionCounter().getCoveredCount()
										+ m.getInstructionCounter().getMissedCount())))
								* 100;
						Double branchCoverage = (Double) (m.getBranchCounter().getCoveredCount() / Double.valueOf(
								(m.getBranchCounter().getCoveredCount() + m.getBranchCounter().getMissedCount())))
								* 100;
						whiteboxCoverageMap.put(Criteria.$LINESCOVERAGE.toString(), linesCoverage);
						whiteboxCoverageMap.put(Criteria.$INSTRUCTIONSCOVERAGE.toString(), instructionsCoverage);
						whiteboxCoverageMap.put(Criteria.$BRANCHESCOVERAGE.toString(), branchCoverage);

					} else {
						// System.out.println("Skipping method: "+m.getName());
					}
				}
			}

		} catch (Exception ex) {

			throw ex;
		}
		return whiteboxCoverageMap;

	}

}
