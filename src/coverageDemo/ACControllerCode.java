package coverageDemo;

import java.util.List;
/**
 * This interface is created so that any test method can dynamically create
 * objects of the classes that implements this interface. For eg, ARWSDAllTransitioins, 
 * ARWSDAllStates, DARWSDAllTransitions, DARWSDAllStates initially create a variable 
 * of type ACControllerCode and then dynamically create objects of ACControllercodeWithBuiltInTest,
 * AcControllerCodeWithoutBuiltInTest and ACControllerFixedCode depending on the user input.
 * @author Ruchi
 *
 */

public interface ACControllerCode {
	
	public  List<Boolean> ac_controller(int message) throws Exception ;
	public boolean isIs_room_hot() ;
		
	public void setIs_room_hot(boolean is_room_hot);
		

	public boolean isIs_door_closed() ;

	public void setIs_door_closed(boolean is_door_closed);

	public boolean isAc();

	public void setAc(boolean ac);
}
