package coverageDemo;

import static org.testng.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jacoco.agent.rt.IAgent;
import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.IMethodCoverage;
import org.jacoco.core.data.ExecutionDataReader;
import org.jacoco.core.data.ExecutionDataStore;
import org.jacoco.core.data.SessionInfoStore;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Class to measure the black box and white box coverage 
 * using EP, BVA and combinations technique after running each test 
 * to check how the coverages progress.
 */

public class EPBVACombinationsCoverageProgress {
	long startTime, endTime, numberOfTestcases;
	
	//Initializing decision tree array to mark all the decision tree paths as uncovered.
	
	boolean decisionTree[] = { false, false, false, false, false, false, false, false, false, false, false, false };
	double decisionTreeCoverage = 0.0;
	int decisionTreeCoveredCount = 0;
	boolean stopTesting = false;
	List<List<Double>> rows = new ArrayList<List<Double>>();
	FileWriter csvWriter;
	
	/*Initializing the test data obtained using EP/BVA/Combinations technique*/
	
	private Object data[][] = 
		{ 
			{ "T1.1", 55, 'F', true, 200 }, 
			{ "T1.2", 20, 'F', true, 300 },
			{ "T1.3", 55, 'M', false, 400 }, 
			{ "T1.4", 35, 'M', false, 500 }, 
			{ "T1.5", 20, 'M', false, 2000 },
			{ "T1.6", 8, 'M', false, 0 }, 
			{ "T1.7", 120, 'M', false, 0 }, 
			{ "T1.8", 20, 'X', true, 0 },
			{ "T2.1", 45, 'F', true, 200 }, 
			{ "T2.2", 16, 'F', true, 300 }, 
			{ "T2.3", 65, 'M', false, 400 },
			{ "T2.4", 44, 'M', false, 500 }, 
			{ "T2.5", 24, 'M', false, 2000 }, 
			{ "T2.6", 15, 'M', false, 0 },
			{ "T2.7", Integer.MIN_VALUE, 'M', false, 0 }, 
			{ "T2.8", 66, 'F', true, 0 },
			{ "T2.9", Integer.MAX_VALUE, 'F', true, 0 }, 
			{ "T2.10", 45, 'X', false, 0 },
			{ "T2.11", 24, 'M', false, 2000 }, 
			{ "T3.1", 55, 'F', false, 200 }, 
			{ "T3.2", 55, 'M', true, 200 },
			{ "T3.3", 35, 'F', false, 300 },
			{ "T3.4", 20, 'F', false, 300 }, 
			{ "T3.5", 35, 'M', true, 300 },
			{ "T3.6", 20, 'M', true, 300 } };

	@DataProvider(name = "ep")
	public Object createdata() {
		try {
			csvWriter = new FileWriter("EPBVACombinationsCoverageProgress.csv");
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return data;
	}

	@Test(dataProvider = "ep")
	public void testPremium(String id, int age, char gender, boolean married, int expectedOutput) throws Exception {

		numberOfTestcases++;
		
		//Checking for the first test to initialize the start time
		
		if (id.equals("T1.1")) {
			startTime = System.currentTimeMillis();
		}
		
		/*
		 * Based on the value of the expected output, decision tree path
		 * is marked as covered and the count of the covered decision tree paths
		 * is incremented.
		 */
		
		if (expectedOutput == 2000) {
			if (decisionTree[0] == false) {
				decisionTree[0] = true;
				decisionTreeCoveredCount++;
			}

		} else if (expectedOutput == 500) {
			if (decisionTree[1] == false) {
				decisionTree[1] = true;
				decisionTreeCoveredCount++;
			}

		} else if (expectedOutput == 400) {
			if (decisionTree[2] == false) {
				decisionTree[2] = true;
				decisionTreeCoveredCount++;
			}

		} else if (expectedOutput == 300) {
			if (decisionTree[3] == false) {
				decisionTree[3] = true;
				decisionTreeCoveredCount++;
			}
			if (!married) {
				if (decisionTree[4] == false) {
					decisionTree[4] = true;
					decisionTreeCoveredCount++;
				}
			} else {
				if (decisionTree[5] == false) {
					decisionTree[5] = true;
					decisionTreeCoveredCount++;
				}
			}
		}

		else if (expectedOutput == 200) {
			if (decisionTree[6] == false) {
				decisionTree[6] = true;
				decisionTreeCoveredCount++;
			}
			if (!married) {
				if (decisionTree[7] == false) {
					decisionTree[7] = true;
					decisionTreeCoveredCount++;
				}

			} else {
				if (decisionTree[8] == false) {
					decisionTree[8] = true;
					decisionTreeCoveredCount++;
				}

			}
		}

		else { // result==ERROR
			if (decisionTree[9] == false) {
				decisionTree[9] = true;
				decisionTreeCoveredCount++;
			}
			if (gender == 'X') {
				if (decisionTree[10] == false) {
					decisionTree[10] = true;
					decisionTreeCoveredCount++;
				}
			} else {
				if (decisionTree[11] == false) {
					decisionTree[11] = true;
					decisionTreeCoveredCount++;
				}
			}

		}
		
		//Chceking for the correctness of the code
		
		assertEquals(Insurance.premium(age, gender, married), expectedOutput);
		List<Double> coverageList = new ArrayList<Double>();
		decisionTreeCoverage = (Double.valueOf(decisionTreeCoveredCount) / Double.valueOf(decisionTree.length)) * 100;
		coverageList.add(Double.valueOf(numberOfTestcases));
		coverageList.add(Double.valueOf(System.currentTimeMillis() - startTime));
		coverageList.add(decisionTreeCoverage);
		List<Double> whiteBoxCoverage = printCoverage("coverageDemo.Insurance", "premium");
		coverageList.add(whiteBoxCoverage.get(0));
		coverageList.add(whiteBoxCoverage.get(1));
		coverageList.add(whiteBoxCoverage.get(2));
		rows.add(coverageList);
		
		// Checking if the last test is reached.
		
		if (id.equals("T3.6")) {

			csvWriter.append("TestCaseNumber");
			csvWriter.append(",");
			csvWriter.append("Execution time in milliseconds");
			csvWriter.append(",");
			csvWriter.append("DecisionTreeCoverage");
			csvWriter.append(",");
			csvWriter.append("LinesCoverage");
			csvWriter.append(",");
			csvWriter.append("InstructionsCoverage");
			csvWriter.append(",");
			csvWriter.append("BranchesCoverage");
			csvWriter.append("\n");
			for (List<Double> rowData : rows) {
				csvWriter.append(String.join(",", rowData.toString().substring(1, rowData.toString().length() - 1)));
				csvWriter.append("\n");
			}

			csvWriter.flush();
			csvWriter.close();

		}
	}

	/**
	 * Method to print the white box coverage
	 * 
	 * @param className
	 * @param methodName
	 * @param refreshExcutiondata
	 * @return
	 * @throws Exception
	 */
	
	public static List<Double> printCoverage(String className, String methodName) throws Exception {
		List<Double> whiteBoxCoverage = new ArrayList<Double>();
		try {
			IAgent agent = org.jacoco.agent.rt.RT.getAgent();
			byte[] executionData = agent.getExecutionData(false);
			CoverageBuilder coverageBuilder = new CoverageBuilder();
			InputStream myInputStream = new ByteArrayInputStream(executionData);
			final ExecutionDataReader reader = new ExecutionDataReader(myInputStream);
			final SessionInfoStore sessionInfoStore = new SessionInfoStore();
			final ExecutionDataStore executionDataStore = new ExecutionDataStore();
			reader.setSessionInfoVisitor(sessionInfoStore);
			reader.setExecutionDataVisitor(executionDataStore);
			reader.read();
			Analyzer analyzer = new Analyzer(executionDataStore, coverageBuilder);
			className = className.replace(".", "/");
			String classFileName = "./bin/" + className + ".class";
			File classFile = new File(classFileName);
			if (!classFile.exists())
				System.out.println("Error: class file not found");
			analyzer.analyzeAll(classFile);
			Collection<IClassCoverage> cc = coverageBuilder.getClasses();

			for (IClassCoverage c : cc) {

				for (IMethodCoverage m : c.getMethods()) {
					if (methodName.equals(m.getName())) {

						Double linesCoverage = (Double) (m.getLineCounter().getCoveredCount() / Double
								.valueOf((m.getLineCounter().getCoveredCount() + m.getLineCounter().getMissedCount())))
								* 100;
						Double instructionsCoverage = (Double) (m.getInstructionCounter().getCoveredCount()
								/ Double.valueOf((m.getInstructionCounter().getCoveredCount()
										+ m.getInstructionCounter().getMissedCount())))
								* 100;
						Double branchesCoverage = (Double) (m.getBranchCounter().getCoveredCount() / Double.valueOf(
								(m.getBranchCounter().getCoveredCount() + m.getBranchCounter().getMissedCount())))
								* 100;

						whiteBoxCoverage.add(linesCoverage);
						whiteBoxCoverage.add(instructionsCoverage);
						whiteBoxCoverage.add(branchesCoverage);

					} else {

					}
				}
			}
		} catch (Exception ex) {

			throw ex;
		}

		return whiteBoxCoverage;
	}
}
