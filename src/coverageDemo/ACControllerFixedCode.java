package coverageDemo;

import java.util.ArrayList;
import java.util.List;

public class ACControllerFixedCode implements ACControllerCode{
	private  boolean is_room_hot = false; /* room is cold */
	private  boolean is_door_closed = false; /* and door is open */
	private  boolean ac = false; /* so, ac is off */

	public  List<Boolean> ac_controller(int message)  {
		List<Boolean> statusList= new ArrayList<Boolean>();
		if (message == 0) {
			is_room_hot = true;
			//Below condition added to fix the code
			if (is_door_closed) {
				ac = true;
				
			} 			
		}
		if (message == 1) {
			is_room_hot = false;
			//Since is_room_hot is assigned value false
			//in this block, ac variable is assigned the value false 
			//to fix the code
			ac=false;
			

		}
		if (message == 2) {
			is_door_closed = false;
			ac = false;
			
		}
		if (message == 3) {
			is_door_closed = true;
			if (is_room_hot) {
				ac = true;
				
			} 
		}		
		statusList.add(is_room_hot);
		statusList.add(is_door_closed);
		statusList.add(ac);
		return statusList;	
	}
	public boolean isIs_room_hot() {
		return is_room_hot;
	}

	public void setIs_room_hot(boolean is_room_hot) {
		this.is_room_hot = is_room_hot;
	}

	public boolean isIs_door_closed() {
		return is_door_closed;
	}

	public void setIs_door_closed(boolean is_door_closed) {
		this.is_door_closed = is_door_closed;
	}

	public boolean isAc() {
		return ac;
	}

	public void setAc(boolean ac) {
		this.ac = ac;
	}
}
