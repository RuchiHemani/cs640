package coverageDemo;

import java.util.ArrayList;
import java.util.List;

public class ACControllerWithoutBuiltInTest implements ACControllerCode {
	private  boolean is_room_hot = false; /* room is not hot */
	private  boolean is_door_closed = false; /* and door is open */
	private  boolean ac = false; /* so, ac is off */

	public  List<Boolean> ac_controller(int message)
			throws Exception {
		List<Boolean> statusList= new ArrayList<Boolean>();
		if (message == 0) {
			is_room_hot = true;
			
		}
		if (message == 1) {
			is_room_hot = false;
			

		}
		if (message == 2) {
			is_door_closed = false;
			ac = false;
			
		}
		if (message == 3) {
			is_door_closed = true;
			if (is_room_hot) {
				ac = true;
				
			} 
		}
		
		// Built-In-Test Removed
		
		statusList.add(is_room_hot);
		statusList.add(is_door_closed);
		statusList.add(ac);
		return statusList;
		

	}

	public boolean isIs_room_hot() {
		return is_room_hot;
	}

	public void setIs_room_hot(boolean is_room_hot) {
		this.is_room_hot = is_room_hot;
	}

	public boolean isIs_door_closed() {
		return is_door_closed;
	}

	public void setIs_door_closed(boolean is_door_closed) {
		this.is_door_closed = is_door_closed;
	}

	public boolean isAc() {
		return ac;
	}

	public void setAc(boolean ac) {
		this.ac = ac;
	}
}
